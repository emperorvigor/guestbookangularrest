Nedan följer koden för att skapa tabellen med värden.

OBS! Skapa först en databas med namnet "guestbookdb".

Råkade slänga upp allt jag hade i gitmappen men den som du ska kolla på är guestbook.

MySQL:
-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Värd: 127.0.0.1
-- Tid vid skapande: 27 maj 2015 kl 12:47
-- Serverversion: 5.6.17
-- PHP-version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databas: `guestbookdb`
--

-- --------------------------------------------------------

--
-- Tabellstruktur `guestinputs`
--

CREATE TABLE IF NOT EXISTS `guestinputs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL DEFAULT 'Anonymous',
  `headline` varchar(45) NOT NULL,
  `message` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumpning av Data i tabell `guestinputs`
--

INSERT INTO `guestinputs` (`id`, `name`, `headline`, `message`) VALUES
(1, 'Anonymous', 'First headline!', 'A simple but awesome message. This be da best, yes?!'),
(2, 'TheChoosen', 'Kawaii', 'This page is so supercute I could hug it all day and all night! Thumps up!'),
(3, 'Someone in the block', 'Freakin´ headline', 'I don´t know what to say...... but I type here anyway!'),
(5, 'Anonymous', 'Hejsan', 'Kul att vara här åäöl');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;