package widar.rest;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import models.Guestinput;
import widar.ejb.GuestbookManager;


@Path("/guestinputs")
public class Guestbook {

    private final GuestbookManager guestbookManager;

    @Inject
    public Guestbook(GuestbookManager guestbookManager) {
        this.guestbookManager = guestbookManager;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Guestinput> getAllGuestinputInJSON() {
        return guestbookManager.getAllInputs();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Guestinput getGuestinputById(@PathParam("id") int id) {
        return guestbookManager.getInputById(id);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Guestinput createGuestinput(Guestinput guestinput) {
        return guestbookManager.createNewInput(guestinput);
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Guestinput updateGuestinput(Guestinput guestinput) {
        return guestbookManager.updateInput(guestinput);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void removeGuestinput(@PathParam("id") int id) {
        guestbookManager.remove(id);
    }
}