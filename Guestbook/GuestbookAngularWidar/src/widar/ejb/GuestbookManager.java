package widar.ejb;

import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import models.Guestinput;

@Stateless
public class GuestbookManager {
	
	@PersistenceContext
	EntityManager em;

	public List<Guestinput> getAllInputs(){
		TypedQuery<Guestinput> query = em.createNamedQuery("Guestinput.findAll", Guestinput.class);
		List<Guestinput> inputList = query.getResultList();
		Collections.reverse(inputList);
		return inputList;
	}
	
	public void addMessage(Guestinput guestInput){
		em.merge(guestInput);
	}

	public Guestinput getInputById(int id) {
		return em.find(Guestinput.class, id);
	}

	public Guestinput createNewInput(Guestinput guestinput) {
		return em.merge(guestinput);
	}

	public Guestinput updateInput(Guestinput guestinput) {
		return em.merge(guestinput);
	}

	public void remove(int id) {
		em.remove(em.find(Guestinput.class, id));		
	}
	
}