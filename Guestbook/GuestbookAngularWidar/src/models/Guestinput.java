package models;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the guestinputs database table.
 * 
 */
@Entity
@Table(name="guestinputs")
@NamedQuery(name="Guestinput.findAll", query="SELECT g FROM Guestinput g")
public class Guestinput implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String headline;

	private String message;

	private String name;

	public Guestinput() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getHeadline() {
		return this.headline;
	}

	public void setHeadline(String headline) {
		this.headline = headline;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}