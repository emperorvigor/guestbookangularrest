angular.module('app.derective.guest', [])
	.directive('guest', function(){
		return{
			restrict: 'E',
			scope: {
				data: '='
			},
			templateUrl: 'guest.html',
			replace: true
		};
	});