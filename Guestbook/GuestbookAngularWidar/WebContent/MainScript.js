var myapp = new angular.module("myapp", ["ngResource", "app.derective.guest"]);
			
			myapp.controller("MainCtrl", ["$scope", "$resource", function($scope, $resource){
				var Guestinput = $resource("/GuestbookAngularWidar/resource/guestinputs/:id", {id: "@id"}, {});
				
				$scope.guestinputs = Guestinput.query();
				
				$scope.remove = function(input){
					Guestinput.get({id: input.id}, function(data){
						$scope.input = data;
						$scope.input.$delete(function(){
							$scope.guestinputs = Guestinput.query();
						})
					})
				}
				
				$scope.newInput = {};
				
				$scope.add = function(newInput){
					
					var guestinput = new Guestinput();
					guestinput.headline = newInput.headline;
					guestinput.message = newInput.message;
					if(!newInput.name == "" || !newInput.name == " "){
						guestinput.name = newInput.name;						
					} else {
						guestinput.name = "Anonymous";
					}
					
					guestinput.$save(function(){
						$scope.guestinputs = Guestinput.query();
					});
				}

			}]);